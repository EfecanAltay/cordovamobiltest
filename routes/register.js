var express = require('express');
var router = express.Router();
var token = require('./token');

var database = require('./dbConnect');
var dM = require('./docManager');


router.get("/",function(req,res,next){
  dM.writeDefaultImage("123");
  res.render('register',{title : 'register'});
  //res.send({message:"register"});
});

  var fakulteler = new Array(
    "Mühendislik Fakültesi",
    "İktisadi ve İdari Bilimler Fakültesi",
    "Fen Edebiyat Fakültesi",
    "Güzel Sanatlar ve Tasarım Fakültesi",
    "İslami İlimler Fakültesi",
    "Ziraat ve Doğa Fakültesi"
  );


    var bolumler = new Array(
      ["Bilgisayar Mühendisliği","Elektrik Elektronik Mühendisliği","İnşaat Mühendisliği","Makine ve imalat Mühendisliği","Kimya ve Süreç Mühendisliği"],
      ["İktisat","İşletme","Maliye","Siyaset Birimi ve Kamu Yönetimi","Yönetim ve Bilişim Sistemleri","Uluslararası ilişkiler"],
      ["Arkeoloji","Coğrafya","İstatistlik","Fizik","Kimya","Matematik","Molerküler Biyoloji ve Genetik","Tarih","Türk Dili Ve Edebiyatı"],
      ["Güzel Sanatlar"],
      ["Felsefe ve Din Bilimleri","İslam Tarihi ve Sanatları","Temel İslam Bilimleri"],
      ["Bahçe Bitkileri","Biyosistem Mühendisliği","Tarla Bitkileri"]
    );

function getBolum(f,b){
  return bolumler[f][b];
}

router.post("/",function(req,res,next){
  var data = req.body ;


  database.connect(function(db){
    while(true){
      var kid = token.getToken();
      var user = db.collection("user").find({kid : kid}).toArray();
      console.log("token wait\n");
      break ;
    }
      console.log("token ready");
      var user = db.collection("user");

      user.insertOne(
        {
          kadi : data.kadi ,
          pass: data.pass ,
          mail : data.mail,
          profileData:
          {
              kid : kid ,
              name : data.name,
              surname :data.surname,
              fakulte : fakulteler[data.fakulte],
              bolum : getBolum(data.fakulte,data.bolum),
              durum : "Kullanıcı",
              img: kid+"/defaultPR.png",
              kimg:kid+"/kres2.jpg",
              friends:[{
                kid : "00",
                name : "Efecan",
                surname : "Altay",
                bolum:  "Bilgisayar Müh",
                img : "img/ic_launcher.png"
              }]
          }
        },
        function(err,result){
          if(err){
            console.log("Kayıt hataya denk geldi\n"+ err );
            res.send({status : false ,message : "Kayıt Hataya denk geldi" });
          }
          else {
            dM.writeDefaultImage(kid);
            res.send({status : true ,message : "Kayıt Başarılı" });
          }
          db.close();
      });

  });

});
router.post("/isRegistered",function(req,res,next){
  var kadi = req.body.kadi ;

  database.connect(function(db){
    var user = db.collection("user").find({kadi : kadi}).toArray(function(err,data){
      if(data.length > 0){
        res.send({status : false});
      }else{
        res.send({status : true});
      }
    });
    db.close();
  });
});
module.exports = router;
