var express = require('express');
var router = express.Router();

var token = require('./token');

var dM = require('./docManager');
var database = require('./dbConnect');

var tokenData = "";

var fs = require("fs");

/* GET users listing. */
router.get('/', function(req, res, next) {

});

router.post('/login',function(req,res,next){
  var kad = req.body.kadi;
  database.connect(function(db){
    var collection = db.collection("user");
    var data = collection.find({kadi : kad}).toArray(function(err,item){
      if(item[0]){
          if(item[0].pass == req.body.pass){
                tokenData = token.getToken();
                var loginUser = db.collection("loginUser");
                loginUser.insert(
                  {
                    "logid" : tokenData ,
                    "profileData" : item[0].profileData
                  },
                  {w:1},
                  function(err,result){
                    if(err){
                      console.log("Kayıt token hatasına denk geldi\n" + err );
                      res.send({message :"Veritabanına Bağlantı hatası" , status:false});
                      db.close();
                    }
                    else {
                      console.log(result);
                      res.send({message :"Giriş Yaptınız.." , logid :tokenData , status:true});
                      db.close();
                    }
                  }
               );
          }
          else{
              res.send({message :"Şifre Yanlış" , status:false});
          }
        }
        else{
            res.send({message :"Kullanıcı Yok" , status:false});
        }
    });
  });
});

router.post('/logout',function(req,res,next){
  database.connect(function(db){
    var collection = db.collection("loginUser");
    var data = collection.remove({logid : req.body.logid});
    res.send({status : true});
    db.close;
    });
});

router.post('/getProfile',function(req,res,next){
  logid = req.body.logid;
  database.connect(function(db){
    var collection = db.collection("loginUser");
    var data = collection.find({logid: logid}).toArray(function(err,item){
      if(item[0]){
        console.log(item[0]._id);
        res.send({status :true, profileData : item[0].profileData });
      }
      else{
        console.log("kullanıcı Yok");
        res.send({status:false});
      }
    });
  });
});
router.post('/setProfileImage',function(req,res,next){
  logid = req.body.logid;
  database.connect(function(db){
    var collection = db.collection("loginUser");
    var data = collection.find({logid: logid}).toArray(function(err,item){
      if(item[0]){
        if(req.body.profImage){
          dM.writeImage(item[0].profileData.kid,req.body.profImage);
          res.send({status :true, profileData : item[0].profileData });
        }else{
          res.send({status :false, msg : "Resim Data Hatalı" });
        }
      }
      else{
        console.log("kullanıcı Yok");
        res.send({status:false});
      }
    });
  });
});


router.post('/getFriends',function(req,res,next){
  logid = req.body.logid;
  database.connect(function(db){
    var collection = db.collection("loginUser");
    var data = collection.find({logid: logid}).toArray(function(err,item){
      if(item[0]){
        if(item[0].profileData.friends)
          res.send({status :true, friendsData: item[0].profileData.friends});
        else
        res.send({status :true, friendsData: ""});
      }
      else{
        console.log("kullanıcı Yok");
        res.send({status:false});
      }
    });

  });
});
router.post('/option/setProfPhoto',function(req,res,next){

});

var profile1 ={
  name : "Efecan",
  surname :"Altay",
  fakulte : "Mühendislik Fakültesi",
  bolum : "Bilgisayar Mühendisliği",
  durum : "Öğrenci",
  img: "img/prof.jpg",
  kimg:"img/kres.jpg",
  status:true
}
var profile2 ={
  name : "Android",
  surname :"Aderson palabıyık",
  fakulte : "Mühendislik Fakültesi",
  bolum : "Sistem Mühendisliği",
  durum : "Yetkili",
  img: "img/ic_launcher.png",
  kimg:"img/kres2.jpg",
  status:true
}
module.exports = router;
