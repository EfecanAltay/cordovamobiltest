var express = require('express');
var router = express.Router();

//115.Satır hatası ifadeyi dışarı aktarma isUser

var database = require('./dbConnect');


router.post("/",function(req,res,next){
  database.connect(function(db){
    var collection = db.collection("newsData");
    collection.find({}).toArray(function(err,item){
      console.log(item);
      res.send({ status : true , message:"News" , news : item });
    });
  });
});
function isUserLogin(rout,callback){
  router.post(rout,function(req,res,next){
    var logid = req.body.logid;
    database.connect(function(db){
      var collection = db.collection("loginUser");
      var data = collection.find({logid : logid}).toArray(function(err,loginUser){
        callback(loginUser,db,req,res);
      });
    });
  });
}
var date = new Date();
isUserLogin("/CreateShare",function(loginUser,db,req,res){
  if(loginUser[0]){
            var news = db.collection("newsData");
            if(req.body.type == 0){
              news.insert(
                {
                  newsType :0,
                  dataType:"Text",
                  bodyText : req.body.bodyText,
                  viewCount: 0,
                  likeCount : 0,
                  date : date.toJSON(),
                  likeUser :[],
                  autherData: {
                    name : loginUser[0].profileData.name,
                    surname : loginUser[0].profileData.surname,
                    img : loginUser[0].profileData.img,
                    bolum : loginUser[0].profileData.bolum
                  },
                  yorumData :{
                       "yorumHeader": req.body.bodyText.split(0,10) + "...",
                       "yorumIcon": [
                           "comment",
                           "picture",
                           "student"
                       ],
                       "yorumIconIndex": 0,
                       "yorumStatus": [
                           "building",
                           "cloud"
                       ],
                       "yorumStatusIndex": 0,
                       "yorums": []
                   }
                },
                {w:1},
                function(err,result){
                  if(err){
                    console.log("Kayıt token hatasına denk geldi\n" + err );
                    res.send({message :"Veritabanına Bağlantı hatası" , status:false});
                    db.close();
                  }
                  else {
                    console.log("result");
                    res.send({message :"Haber Oluşturuldu" , status:true});
                    db.close();
                  }
                }
             );
           }else if (req.body.type == 1) {
             news.insert(
               {
                 newsType :0,
                 dataType:"Activity",
                 bodyText : req.body.bodyText,
                 viewCount: 0,
                 likeCount : 0,
                 likeUser :[],
                 ActivityDate:req.body.date,
                 time :req.body.time,
                 date : date.toJSON(),
                 autherData: {
                   name : loginUser[0].profileData.name,
                   surname : loginUser[0].profileData.surname,
                   img : loginUser[0].profileData.img,
                   bolum : loginUser[0].profileData.bolum
                 },
                 yorumData :{
                      "yorumHeader": req.body.bodyText.split(0,10) + "...",
                      "yorumIcon": [
                          "comment",
                          "picture",
                          "student"
                      ],
                      "yorumIconIndex": 0,
                      "yorumStatus": [
                          "building",
                          "cloud"
                      ],
                      "yorumStatusIndex": 0,
                      "yorums": []
                  }
               },
               {w:1},
               function(err,result){
                 if(err){
                   console.log("Kayıt token hatasına denk geldi\n" + err );
                   res.send({message :"Veritabanına Bağlantı hatası" , status:false});
                   db.close();
                 }
                 else {
                   console.log("result");
                   res.send({message :"Haber Oluşturuldu" , status:true});
                   db.close();
                 }
               }
            );
           }

    }
    else{
        res.send({message :"Kullanıcı Yok" , status:false});
    }
});
isUserLogin("/likeShare",function(loginUser,db,req,res){
  if(loginUser[0]){
            var news = db.collection("newsData");
            news.find({ '_id' : database.ObjectId(req.body.newsId)}).toArray(function(err,data){
              var kid = loginUser[0].profileData.kid;
              if(data[0]){
                var likeUser = data[0].likeUser;
                console.log(likeUser.length);
                var isUser = false;
                for(var i = 0 ; i < likeUser.length ; i++)
                {
                  if(likeUser[i] == kid)
                    {
                      isUser = true;
                      break;
                    }
                }
                if(!isUser){
                    news.update(
                      {'_id' : database.ObjectId(req.body.newsId)},
                      {
                        $set:{likeCount : likeUser.length+1 },
                        $push :{likeUser : kid }
                      },
                      { multi: true},
                      function(err,updated){
                        if(err){
                            res.send({status:false});
                            console.log("err");
                          }
                        else{
                          //console.log("kullanıcı beğendi.");
                          res.send({status:true});
                          }
                      }
                   );
                 }else{
                   news.update(
                     {'_id' : database.ObjectId(req.body.newsId)},
                     {
                       $set:{likeCount : likeUser.length-1 },
                       $pull :{likeUser : kid }
                     },
                     { multi: true},
                     function(err,updated){
                       if(err){
                           console.log("err");
                           res.send({status:false});
                         }
                       else{
                         //console.log("kullanıcı beğenmekten vazgecti.");
                         res.send({status:true});
                         }
                     }
                   );
                 }
                db.close();
              }else{
                console.log("haber yok");
                res.send({status:false});
                db.close();
              }
            });
    }
    else{
      console.log("kullanıcı yok");
        res.send({message :"Kullanıcı Yok" , status:false});
    }
});
isUserLogin("/getYorums",function(loginUser,db,req,res){
  if(loginUser[0]){
    var news = db.collection("newsData");
    news.find({ '_id' : database.ObjectId(req.body.newsId)}).toArray(function(err,data){
      if(data[0])
        res.send({status:true, yorumData:data[0].yorumData});
      else {
        res.send({status:false , message:"yorum yapılacak haber nesnesi bulunamadı"});
      }
    });
  }
});
isUserLogin("/createYorum",function(loginUser,db,req,res){
  if(loginUser[0]){
    var news = db.collection("newsData");
    news.find({ '_id' : database.ObjectId(req.body.newsId)}).toArray(function(err,data){
      news.update(
        {'_id' : database.ObjectId(req.body.newsId)},
        {
          $push :{"yorumData.yorums" :   {
            count: data[0].yorumData.yorums.length,
            author : loginUser[0].profileData.kid,
            user : {
                    name: loginUser[0].profileData.name,
                    profImg : loginUser[0].profileData.img
                   },
            text : req.body.text,
            date : date.toJSON()
            }
          }
        },
        { multi: true},
        function(err,updated){
          if(err){
              res.send({status:false});
              console.log("err");
              db.close();
            }
        }
     );
     news.find({ '_id' : database.ObjectId(req.body.newsId)}).toArray(function(err,data){
       if(data[0]){
         res.send({status:true, yorumData : data[0].yorumData});
         console.log(data[0]);
       }
       else {
         res.send({status:false});
       }
       db.close();
     });
    });
  }
  else{
      console.log("kullanıcı yok");
      db.close();
      res.send({message :"Kullanıcı Yok" , status:false});
  }
});


var data=[{
  newsType :0,
  dataType:"Text",
  bodyText : "Merhabalar Ben Efecan Altay.\n Sizlere bu Sosyal Paylaşım Platformunu kurdum.",
  bodyImage: "Resimm",
  viewCount: 5,
  likeCount : 3,
  date : "07.05.2016",
  autherData: {
    name : "",
    surname : "",
    img : "",
    bolum : ""
  },
  yorumData : {
    yorumHeader:"Merhabalar ben ...",
    yorumIconIndex:0,
    yorumIcon:["comment" , "picture" ,"student"],
    yorumStatusIndex:0,
    yorumStatus:["building","cloud"],
    yorums : [
      {
      count: 0,
      your : true,
      user : {
              name: "Efecan Altay",
              profImg : "img/prof.jpg"
             },
      text : "Yorumları Alalım",
      date : "23.12.07",
      time : "23.00"
      },
      {
      count: 1,
      your : false,
      user : {
              name: "Android",
              profImg : "img/ic_launcher.png"
              },
      text : "Çok Güzel bir uygulama",
      date : "23.12.07",
      time : "23.00"
      },
      {
      count: 2,
      your : false,
      user : {
              name: "Bilecik",
              profImg : "img/bileciklogo.jpg"
              },
      text : "Bende Evet Katılıyorum",
      date : "23.12.07",
      time : "23.00"
      }
  ]
}
},
{
  newsType :0,
  dataType:"Photo",
  bodyText : "Merhabalar Ben Efecan Altay.\n Sizlere bu Sosyal Paylaşım Platformunu kurdum.",
  bodyImage: "Resimm",
  viewCount: 10,
  likeCount : 5,
  date : "07.05.2016",
  yorumData : {
    yorumHeader:"Merhabalar ben ...",
    yorumIconIndex:1,
    yorumIcon:["comment" , "picture" ,"student"],
    yorumStatusIndex:0,
    yorumStatus:["building","cloud"],
    yorums : [{
      count: 0,
      your : false,
      user : {
          name: "Android",
          profImg : "img/ic_launcher.png"
      },
      text : "Resim Bi harika",
      date : "23.12.07",
      time : "23.00"
      },{
      count: 1,
      your : true,
      user : {
          name: "Efecan Altay",
          profImg : "img/prof.jpg"
      },
      text : "Teşekkür Ederim",
      date : "23.12.07",
      time : "23.00"
    }]
  }
},
{
  newsType :0,
  dataType:"Activity",
  bodyImage: "Resim",
  activityInfo :{name:"Akıllı Cihazların Önemi",date:"23 Ekim 2016",time:"20:00"},
  viewCount: 8,
  likeCount : 3,
  date : "07.05.2016",
  yorumData : {
    yorumHeader:"Merhabalar ben ...",
    yorumIconIndex:2,
    yorumIcon:["comment" , "picture" ,"student"],
    yorumStatusIndex:0,
    yorumStatus:["building","cloud"],
    yorums : [{
      count: 0,
      your : true,
      user : {
          name: "Efecan Altay",
          profImg : "img/prof.jpg"
      },
      text : "Çok Güzel Bir etkinlikti",
      date : "23.12.07",
      time : "23.00"
      },{
      count: 1,
      your : false,
      user : {
          name: "Efecan Altay",
          profImg : "img/prof.jpg"
      },
      text : "Evet Katılıyorum",
      date : "23.12.07",
      time : "23.00"
    }]
  }
}];



module.exports = router;
