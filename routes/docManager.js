
var fs = require("fs");


var setDefaultImage = function(kid){
  var defaultImage =  fs.readFileSync('UserData/default/defaultPR.png');
  var dir = "UserData/"+kid;
  fs.access(dir,function(err){
    if(err)
      fs.mkdirSync(dir);
  });

  fs.unlink(dir, function(err){
    var options = { flag : 'w' };
    if(defaultImage)
      fs.writeFile(dir+"/defaultPR.png",defaultImage,options, function(err) {
        if(err)
        console.error("!!!hatalı yazma : profil resmi kopyalama başarısız");
        else {
          console.log("profil resmi kopyalama başarılı");
        }
      });
    else {
      console.error("Image Yok");
    }
  });
}

var setImage = function(kid,ImageData){
  var profImageData = new Buffer(ImageData, "base64") ;
  var dir = "UserData/"+kid;
  fs.access(dir,function(err){
    if(err)
      fs.mkdirSync(dir);
  });

  fs.unlink(dir, function(err){
    var options = { flag : 'w' };
    if(profImageData)
      fs.writeFile(dir+"/defaultPR.png",profImageData,options, function(err) {
        if(err)
        console.error("!!!hatalı yazma : profil resmi kopyalama başarısız");
        else {
          console.log("profil resmi kopyalama başarılı");
        }
      });
    else {
      console.error("Image Yok");
    }
  });
}


var doc = {
  writeDefaultImage : setDefaultImage,
  writeImage: setImage
}
module.exports = doc;
